import React from "react"
import { navigate } from "gatsby"

const LinkButton = props => {
  return (
    <button
      class="linkbutton"
      onClick={event => {
        event.preventDefault()
        navigate(props.link)
      }}
    >
      {props.text}
    </button>
  )
}

export default LinkButton
