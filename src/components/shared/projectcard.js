import React from "react"
import Image from "../../components/image"

const ProjectCard = props => {
  return (
    <a
      class="projectcard"
      href={props.link}
      alt="project"
      target="_blank"
      rel="noreferrer"
    >
      <Image class="projectcard__image" alt="project" filename={props.image} />
      <div class="projectcard__info">
        <h2 class="projectcard__info__title">
          {props.title}{" "}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            class="bi bi-box-arrow-up-right"
            viewBox="0 0 16 16"
          >
            <path
              fill-rule="evenodd"
              d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"
            />
            <path
              fill-rule="evenodd"
              d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"
            />
          </svg>
        </h2>
        <p class="projectcard__info__text">{props.text}</p>
        <p class="projectcard__info__small">{props.small}</p>
      </div>
    </a>
  )
}

export default ProjectCard
