import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const AboutPage = () => (
  <Layout>
    <SEO title="About" />
    <div class="about container-fluid p-0">
      <div class="about__hello">
        <div class="about__hello__inner container">
          <div class="about__hello__inner__top">
            <h2 class="about__hello__inner__top__title">About</h2>
            <div class="about__hello__inner__top__cv">
              <p class="about__hello__inner__top__cv__text">Download my CV:</p>
              <a href="/downloads/Liam Best CV.pdf" download="Liam Best CV.pdf">
                <button class="about__hello__inner__top__cv__button">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    class="bi bi-cloud-download-fill"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M8 0a5.53 5.53 0 0 0-3.594 1.342c-.766.66-1.321 1.52-1.464 2.383C1.266 4.095 0 5.555 0 7.318 0 9.366 1.708 11 3.781 11H7.5V5.5a.5.5 0 0 1 1 0V11h4.188C14.502 11 16 9.57 16 7.773c0-1.636-1.242-2.969-2.834-3.194C12.923 1.999 10.69 0 8 0zm-.354 15.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 14.293V11h-1v3.293l-2.146-2.147a.5.5 0 0 0-.708.708l3 3z"
                    />
                  </svg>
                </button>
              </a>
            </div>
          </div>
          <p class="about__hello__inner__text">
            I am a creative and driven full-stack web developer and designer
            from London currently based in Berlin, Germany. I am dedicated to
            designing, building and optimising high-impact user-centric websites
            and have previous experience in business and marketing within the
            e-commerce and advertising sectors.
          </p>
        </div>
      </div>
      <div class="about__coding">
        <div class="about__coding__inner container">
          <h2 class="about__coding__inner__title">Development</h2>
          <h3 class="about__coding__inner__subtitle">Front-end</h3>
          <p class="about__coding__inner__text">
            My front-end skills focus on the React and Vue Javascript frameworks
            in order to make powerful, interactive web applications. I also make
            use of the Gatsby framework (built on React) to build lightning-fast
            static websites.
            <br />
            When it comes to design, I opt to make use of the Sass CSS framework
            to build websites which are easily styled and can be designed in a
            scalable way.
          </p>
          <p class="about__coding__inner__text">
            My full set of front-end skills include:
          </p>
          <ul class="about__coding__inner__list">
            <li>HTML</li>
            <li>CSS + SASS Framework</li>
            <li>Bootstrap</li>
            <li>Javascript + React and Vue Frameworks</li>
          </ul>
          <h3 class="about__coding__inner__subtitle">Back-end</h3>
          <p class="about__coding__inner__text">
            When it comes to back-end I have experience with both SQL and no-SQL
            databases, namely MySQL and MongoDB and have built applications on a
            back-end comprising of the Laravel framework based on PHP.
          </p>
          <p>My full set of back-end skills include:</p>
          <ul class="about__coding__inner__list">
            <li>PHP + Laravel Framework</li>
            <li>MySQL</li>
            <li>MongoDB</li>
          </ul>
        </div>
      </div>
      <div class="about__design">
        <div class="about__design__inner container">
          <h2 class="about__design__inner__title">Design</h2>
          <p class="about__design__inner__text">
            I possess a range of design skills from graphic design to photo
            editing and building website wireframes and concepts. I have
            experience designing website pages, building advertising banners and
            posters as well as touching-up and making content ready for the web.
          </p>
          <p class="about__design__inner__text">
            My full set of design skills include:
          </p>
          <ul class="about__design__inner__list">
            <li>Figma</li>
            <li>Abobe Photoshop</li>
            <li>GIMP</li>
            <li>Inkscape</li>
          </ul>
        </div>
      </div>
    </div>
  </Layout>
)

export default AboutPage
