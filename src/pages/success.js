import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const SuccessPage = () => (
  <Layout>
    <SEO title="Success" />
    <div class="contact container-fluid p-0">
      <div class="contact__intro">
        <div class="contact__intro__inner container">
          <h2 class="contact__intro__inner__title" style={{ color: "#4BB543" }}>
            Thanks! Your message was successfully sent.
          </h2>
          <h2 class="contact__intro__inner__title">Contact</h2>
        </div>
      </div>
      <div class="contact__form">
        <div class="contact__form__inner container">
          <form
            class="contact__form__inner__form"
            name="contact"
            method="POST"
            data-netlify="true"
            action="/success"
          >
            <div class="contact__form__inner__form__section">
              <label
                htmlFor="name"
                class="contact__form__inner__form__section__label"
              >
                Your Name:
              </label>
              <input
                class="contact__form__inner__form__section__input"
                type="text"
                name="name"
                id="name"
              />
            </div>
            <div class="contact__form__inner__form__section">
              <label
                htmlFor="email"
                class="contact__form__inner__form__section__label"
              >
                Your Email:
              </label>
              <input
                class="contact__form__inner__form__section__input"
                type="email"
                name="email"
                id="email"
              />
            </div>
            <div class="contact__form__inner__form__section">
              <label
                htmlFor="message"
                class="contact__form__inner__form__section__label"
              >
                Your Message:
              </label>
              <textarea
                class="contact__form__inner__form__section__textarea"
                name="message"
                id="message"
              ></textarea>
            </div>
            <button class="linkbutton" type="submit">
              Send
            </button>
          </form>
        </div>
      </div>
    </div>
  </Layout>
)

export default SuccessPage
